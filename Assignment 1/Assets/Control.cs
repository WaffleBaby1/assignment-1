﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Control : MonoBehaviour {
	public float speed;
	private int count;
	public Text countText;
	private float horizontal;
	private float vertical;
	public float tiltAngle;
	// Use this for initialization
	void Start () {
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += new Vector3 (Input.GetAxis ("Horizontal"), 0.0f, Input.GetAxis ("Vertical"));
		horizontal = Input.GetAxis ("Horizontal") * tiltAngle;
		vertical = Input.GetAxis ("Vertical");
		Quaternion target = Quaternion.Euler (0, horizontal, 0);
		transform.rotation = Quaternion.Slerp (transform.rotation, target, Time.deltaTime);
	}
		void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Well")) {
			other.gameObject.SetActive (false);
			count = count + 1;
			SetCountText ();
				
		}
		if (other.gameObject.CompareTag ("Enemy")) {
			other.gameObject.SetActive (false);
			count = count - 1;
			SetCountText ();
		}
	}
		void SetCountText ()
		{
			countText.text = "Count: " + count.ToString ();
			if (count >= 8) {
				countText.text = "You Win!";
			}
		}

}
